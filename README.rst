BasGra
=======

Python development version of **BASGRA** model


.. inclusion-marker-do-not-remove


How to test
=============

- Run `bash run.sh`
- A set of graphs will appear from the run.

How to use BasGra software
==========================

You need to install:

- Python (> 3.10) 
- Numerical Python *(pip install numpy)*

The source code comes with:

- `run_basgra.py` that you can run with `bash run.sh` directly.


:Output:

A set of graphs will be written to a PNG (`basgra.png`) from the run.

.. image:: ../../basgra.png
  :width: 800



How to contribute
=================

Please contact the Agri4Cast_ team

.. _Agri4cast: eu-agri4cast-info@ec.europa.eu

JRC, Ispra, Italy

Other related pasture models
============================
- PaSim [INRAE, Clermont]
- MODVEGE [https://code.europa.eu/agri4cast/modvege]
- LINGRA [https://code.europa.eu/agri4cast/lingra]


# Read input parameters
from lib_read_input_files import *
# Internal basgra functions
from lib_basgra import *
# basgra model as a function
from basgra import *
# Read arguments given to this script
import argparse
parser = argparse.ArgumentParser(description="Runs the BASGRA simulation")
parser.add_argument("params_fname", default='params.csv', help="The CSV file holding the input parameters")
parser.add_argument("weather_fname", default='weather.csv', help="The CSV file holding the weather data, the RS data (Evap, tran, LAI & cut dates)")
parser.add_argument("startdoy", default=227, type=int, help="The DOY at which the simulation starts")
parser.add_argument("enddoy", default=365, type=int, help="The DOY at which the simulation stops")
parser.add_argument("startyear", default=1999, type=int, help="The YEAR at which the simulation starts")
parser.add_argument("endyear", default=2000, type=int, help="The YEAR at which the simulation stops")
args = parser.parse_args()

# Input definition 
#params_fname = 'params.csv'
#weather_fname = 'weather.csv'
#startdoy = 227
#enddoy = 365
#startyear = 1999
#endyear = 2000

# Create out_dates list
import datetime
out_dates = []
d = datetime.date(args.startyear,1,1)+datetime.timedelta(days=args.startdoy)
while d <= datetime.date(args.endyear,1,1)+datetime.timedelta(days=args.enddoy):
    out_dates.append(d)
    d += datetime.timedelta(days=1)

# Read Params file (params.csv) and create array
params = read_params(args.params_fname)
# Read Weather file (weather.csv) and create array
weather = read_weather(args.weather_fname)

def run_basgra(params, weather, startdoy, enddoy, startyear, endyear):
    # Run BASGRA
    #ph, evp, trn, tl_v, tl_n, tl_h, gv_b, dv_b, h_b = basgra(params, weather, startdoy, enddoy, startyear, endyear)
    nell_vg, laim, ph, evp, trn, tl_v, tl_n, tl_h, gv_b, dv_b, h_b, d_m, tranrf = basgra(params, weather, startdoy, enddoy, startyear, endyear)

    # Input CALVAL #### ONLY FOR DEBUG !
    calval_fname = 'calval.csv'
    # Read CALVAL #### ONLY FOR DEBUG !
    calval = read_calval(calval_fname)
    #YEAR,DOY,RES,DM,LAI,LERG,NELLVG,RLEAF,SLA,TILTOT,FRTILG,CP
    # calendar & weather
    year    = calval[:,0]  # YYYY
    doy     = calval[:,1]  # DOY
    res     = calval[:,2]  # RES ?
    dm      = calval[:,3]  # DM [kg/ha?]
    lai     = calval[:,4]  # LAI [-]
    lerg    = calval[:,5]  # LERG: Elongation rate of leaves on elongating tillers (m/day)
    nellvg  = calval[:,6]  # NELLVG: Number of growing leaves per elongating tiller
    rleaf   = calval[:,7]  # RLEAF: reductions in leaf appearance rate
    sla     = calval[:,8]  # SLA: specific leaf area 
    tiltot  = calval[:,9]  # Tillers Total 
    frtilg  = calval[:,10] # FRTILG ? (a single value of 0.8 in a year...)
    cp      = calval[:,11] # CP ?

    # Create the Harvesting list
    harvested = [0] * len(dm)
    # Fill it with 2000150
    harvested[288] = dm[288]-dm[289]
    for i in range(288,288+66,1):
        harvested[i] = harvested[288]
    # Fill it with 2000216
    harvested[354] = dm[354]-dm[355]
    for i in range(355,len(harvested),1):
        harvested[i] = harvested[354]

    #resize out_dates to actual output size
    out_d = out_dates[:len(gv_b)]

    #Plot to basgra.png by default
    from lib_write_output_files import plot
    out = plot(out_d, dm, lai, nellvg, tiltot, nell_vg, laim, ph, evp, trn, tl_v, tl_n, tl_h, gv_b, dv_b, h_b, harvested, d_m, tranrf)

# run the main function
run_basgra(params, weather, args.startdoy, args.enddoy, args.startyear, args.endyear)

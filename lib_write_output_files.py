
def plot(out_d, dm, lai, nellvg, tiltot, nell_vg, laim, ph, evp, trn, tl_v, tl_n, tl_h, gv_b, dv_b, h_b, harvested, d_m, tranrf):
    """
    Plotting function, by default writes to basgra.png
    """
    import numpy as np
    import matplotlib.pyplot as plt

    phenrf = np.multiply(np.subtract(1,ph),1.981017)
    phenrf_re = np.divide(nell_vg,2.09178)

    plt.figure(figsize=(15,7))

    plt.subplot(331)
    plt.plot(out_d,d_m,'m-',label="d_m")
    plt.plot(out_d,dm[:len(out_d)],'g-',label="dm ref")
    plt.title('Green Vegetative biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(332)
    plt.plot(out_d,np.add(evp,trn),'c-',label="ET")
    plt.plot(out_d,evp,'b-',label="evaporation")
    plt.plot(out_d,trn,'g-',label="transpiration")
    plt.title('Evaporation and Transpiration (mm/d)')
    plt.legend()
    plt.grid()

    plt.subplot(333)
    #tl_h is tilg2 in basgra (number of elongating tillers)
    #tl_v is tilv in basgra (number of tillers?)
    plt.plot(out_d,np.add(np.add(tl_v,tl_h),tl_n),'r-',label="tillers")
    #plt.plot(out_d,tl_v,'r-',label="tillers")
    plt.plot(out_d,tiltot[:len(out_d)],'b-',label="tillers ref")
    plt.title('Tillers')
    plt.legend()
    plt.grid()

    plt.subplot(334)
    plt.plot(out_d,gv_b,'m-',label="gv_b")
    plt.plot(out_d,dv_b,'g-',label="dv_b")
    #plt.plot(out_d,out_dvb,'b-',label="out_dvb")
    plt.title('Green & Dead Vegetative biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(335)
    plt.plot(out_d,ph,'b-',label="phen")
    plt.plot(out_d,phenrf,'g-',label="phenrf ext. calc.")
    plt.plot(out_d,phenrf_re,'m-',label="phenrf rev Engg")
    plt.title('Phenology')
    plt.legend()
    plt.grid()

    plt.subplot(336)
    plt.plot(out_d,tl_n,'b-',label="tilg1: Non-elong generative")
    plt.plot(out_d,tl_h,'g-',label="tilg2: Elong. generative")
    plt.plot(out_d,tl_v,'r-',label="tilv: Vegetative")
    plt.plot(out_d,np.add(np.add(tl_v,tl_h),tl_n),'k-',label="tillers sum Total")
    plt.title('Tillers Count (tillers/m2)')
    plt.legend()
    plt.grid()

    plt.subplot(337)
    plt.plot(out_d,laim,'m-',label="LAI")
    plt.plot(out_d,lai[:len(out_d)],'g-',label="LAI ref")
    plt.plot(out_d,tranrf,'b-',label="TranRf")
    plt.title('LAI (Forced, should always be strictly equal)')
    plt.legend()
    plt.grid()

    # Harvested Biomass Plot
    plt.subplot(338)
    plt.plot(out_d,h_b,'m-',label="h_b")
    plt.plot(out_d,harvested[:len(out_d)],'g-',label="h_b ref")
    plt.title('Harvested biomass (kg DM/ha)')
    plt.legend()
    plt.grid()

    plt.subplot(339)
    plt.plot(out_d,nell_vg,'m-',label="nellvg")
    plt.plot(out_d,nellvg[:len(out_d)],'g-',label="nellvg ref")
    #plt.plot(out_d,tl_h,'c-',label="tillers harvestable")
    plt.title('NELL VG')
    plt.legend()
    plt.grid()

    plt.tight_layout()
    plt.savefig('basgra.png')
    # Uncomment if you need to show the plot
    #plt.show()
